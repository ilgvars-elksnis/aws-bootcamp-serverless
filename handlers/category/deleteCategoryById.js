import Category from "../../utils/dynamo/categories";
import { validate } from "uuid";

export default async (event) => {
  try {
    // validate the orderId
    const categoryId = event.pathParameters.categoryId;
    if (validate(categoryId) === false) {
      return {
        statusCode: 400,
        body: JSON.stringify("Invalid ID supplied"),
      };
    }

    const category = await Category.query("id").eq(categoryId).exec();
    if (category === undefined || category.count === 0) {
      /* no order found in table */
      return {
        statusCode: 404,
        headers: {},
        body: "No category found",
        isBase64Encoded: false
      };
    }
    //delete the order itself if it exists;
    await Category.delete(categoryId);
    return {
      statusCode: 200,
      body: JSON.stringify(`deleted category with id: ${categoryId} (${category[0]?.name})`),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};

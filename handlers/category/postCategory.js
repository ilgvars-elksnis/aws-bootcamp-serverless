import Category from '../../utils/dynamo/categories';

export default async (event) => {
  try {
    const body = JSON.parse(event.body);

    const exists = await Category.query("name").eq(body.name).exec();
    if(exists !== undefined && exists.count > 0) {
        return {
            statusCode: 400,
            body: "such category already exists"
        };
    }

    const category = await Category.create(body);

    return {
      statusCode: 200,
      body: JSON.stringify(category),
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
        statusCode: 500,
        headers: {},
        body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
        isBase64Encoded: false
    };
  }
};

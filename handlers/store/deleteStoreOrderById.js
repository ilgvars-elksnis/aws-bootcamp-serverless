import Order from "../../utils/dynamo/orders";

export default async (event) => {
  try {
    // validate the orderId
    // const orderId = Number(event.pathParameters.orderId);
    // if (orderId < 0 || Number.isNaN(orderId)) {
    //     return {
    //       statusCode: 400,
    //       body: JSON.stringify("Invalid ID supplied"),
    //     };
    // }

    //await Order.query("id").delete(orderId).exec();

    const orderId = event.pathParameters?.orderId;
    const order = await Order.get(orderId);

    if (!order) {
        return {
            statusCode: 404,
            body: "order not found"
        };
    }

    await Order.delete(order.id);
    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify("deleted store order by id"),
      isBase64Encoded: false
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
import User from "../../utils/dynamo/users";

export default async (event) => {
  try {
    const username = event.pathParameters?.username;
    const user = await User.query("username").eq(username).exec();

    if (!user || user.count < 1) {
        return {
            statusCode: 404,
            body: "User not found"
        };
    }

    const respUser = await User.get(user[0].id);

    delete respUser.hash;
    delete respUser.salt;

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify(user[0]),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
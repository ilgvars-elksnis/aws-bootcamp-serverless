import User from "../../utils/dynamo/users";
import crypto from "crypto";

export default async (event) => {
  try {
    const errResp = {
        statusCode: 400,
        body: "username or password is incorrect, please try again"
    };

    const username = event.queryStringParameters?.username;
    const password = event.queryStringParameters?.password;
    if (!username || !password) {
        return errResp;
    }

    const user = (await User.query("username").eq(username).limit(1).exec())[0];
    if (!user || user.count < 1) {
      return errResp;
    }
    const fullUser = await User.get(user.id);

    const hash = crypto.pbkdf2Sync(password, fullUser.salt, 1000, 64, `sha512`).toString(`hex`);

    if (fullUser.hash !== hash) {
        return errResp;
    }

    await User.update({id: user.id, loggedIn: true });

    return {
      statusCode: 200,
      headers: {},
      body: JSON.stringify("Success"),
      isBase64Encoded: false
    };
  } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};
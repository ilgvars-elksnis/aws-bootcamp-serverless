import { aws, Schema, model } from 'dynamoose';
import { v4 as uuidv4 } from 'uuid';

/* If we are on local - connect to local table */
if (process.env.STAGE === "local") {
  aws.ddb.local(process.env.DYNAMO_URL);
}


/* Create the schema - how the table data should look) */
const  OrderSchema = new Schema({
  "id": {
    "type": String,
    "hashKey": true,
    default: uuidv4()
  },
  "petId": { "type": String, required: true },
  "quantity": { "type": Number, required: true },
  "shipDate": { "type": Date, required: false },
  "status": {
    "type": String,
    "required": true,
    "index": {
      "name": "OrderStatusIndex",
      "global": true,
    },
    "rangeKey": true,
    "enum": ["available", "pending", "sold"]
  },
  "complete": { "type": Boolean },
});

/* verify that all the env variables are set in .env file */
if (!process || !process.env || !process.env.ORDERS_TABLE_NAME) {
  throw new Error('no Orders table name configured');
}
if (!process || !process.env || !process.env.SERVICE) {
  throw new Error('no service name configured');
}
if (!process || !process.env || !process.env.STAGE) {
  throw new Error('no stage configured');
}

// const stage = process.env?.STAGE;
const service = process.env?.SERVICE;
const stage = process.env?.STAGE;
const table = process.env?.ORDERS_TABLE_NAME;

/* this is how we access the data in the table, by providing table name and schema */
const Order = model(`${service}-${stage}-${table}`, OrderSchema);

export default Order;
